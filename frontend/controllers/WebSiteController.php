<?php

namespace frontend\controllers;

use Yii;
use common\models\WebSite;
use common\models\Category;
use common\models\WebSiteGallery;
use common\models\Rates;
use app\models\SearchWebSite;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * WebSiteController implements the CRUD actions for WebSite model.
 */
class WebSiteController extends BaseController {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
//                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all WebSite models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new SearchWebSite();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }
    public function actionList() {
        $searchModel = new SearchWebSite();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single WebSite model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new WebSite model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new WebSite();
        $galleryModel = new WebSiteGallery();
        $categoryModel = Category::find()->all();
        $model->creator_id = \Yii::$app->user->identity->id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $galleryModel->site_id = $model->id;
            if ($galleryModel->load(Yii::$app->request->post())) {
                $uploadedImages = json_decode(Yii::$app->request->post("WebSiteGallery")["uploadedFiles"]);
                $deletedImages = json_decode(Yii::$app->request->post("WebSiteGallery")["deletedFiles"]);
                if ($deletedImages != null) {
                    foreach ($deletedImages as $deletedImage) {
                        unlink($deletedImage->targetFile);
                    }
                }
                if ($uploadedImages != null) {
                    foreach ($uploadedImages as $uploadedImage) {
                        $newImage = new WebSiteGallery();
                        $newImage->site_id = $model->id;
                        $newImage->image = $uploadedImage->fileName;

                        rename($uploadedImage->targetFile, $_SERVER['DOCUMENT_ROOT'] . "frontend/web/uploads/web_site_images/" . $uploadedImage->fileName);
                        $newImage->save();
                    }
                }
                $mainImage = Yii::$app->request->post()["WebSiteGallery"]["mainImage"];
                $siteMainImage = WebSiteGallery::find()->where(['image' => $mainImage])->one();
                if ($siteMainImage) {
                    $siteMainImage->main = 1;
                    $siteMainImage->save();
                }
            }
            return $this->redirect(['view', 'id' => $model->id, 'galleryModel' => $galleryModel]);
        } else {
            return $this->render('create', [
                        'model' => $model,
                        'categoryModel' => $categoryModel,
                        'galleryModel' => $galleryModel
            ]);
        }
    }

    /**
     * Updates an existing WebSite model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $galleryModel = WebSiteGallery::findAll(["site_id" => $id]);
        if (!\Yii::$app->user->isGuest && \Yii::$app->user->identity->id == $model->creator_id) {
            $categoryModel = Category::find()->all();
            if ($galleryModel == null) {
                $galleryModel = new WebSiteGallery();
            }
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                if (Yii::$app->request->post()["WebSiteGallery"]) {


                    $uploadedImages = json_decode(Yii::$app->request->post()["WebSiteGallery"]["uploadedFiles"]);
                    $deletedImages = json_decode(Yii::$app->request->post()["WebSiteGallery"]["deletedFiles"]);
                    $deleteUploadedImages = json_decode(Yii::$app->request->post()["WebSiteGallery"]["deleteUploadedImages"]);

                    if ($deletedImages != null) {
                        foreach ($deletedImages as $deletedImage) {
                            unlink($deletedImage->targetFile);
                        }
                    }
                    if ($deleteUploadedImages != null) {
                        foreach ($deleteUploadedImages as $deleteUploadedImage) {
                            WebSiteGallery::deleteAll(["id" => $deleteUploadedImage->id]);
                            unlink($_SERVER['DOCUMENT_ROOT'] . "frontend/web/uploads/web_site_images/" . $deleteUploadedImage->image);
                        }
                    }
                    if ($uploadedImages != null) {
                        foreach ($uploadedImages as $uploadedImage) {

                            $image = new WebSiteGallery();
                            $image->site_id = $id;
                            $image->image = $uploadedImage->fileName;
                            rename($uploadedImage->targetFile, $_SERVER['DOCUMENT_ROOT'] . "uploads/web_site_images/" . $uploadedImage->fileName);

                            $image->save();
                        }
                    }
                    $mainImage = Yii::$app->request->post()["WebSiteGallery"]["mainImage"];
                    $siteMainImage = WebSiteGallery::find()->where(['image' => $mainImage])->one();
//                var_dump($mainImageId);die;
                    if ($siteMainImage) {

                        $siteMainImage->main = 1;
                        $siteMainImage->save();
                    }
                }
                return $this->redirect(['view', 'id' => $model->id, 'categoryModel' => $categoryModel]);
            } else {
                return $this->render('update', [
                            'model' => $model,
                            'categoryModel' => $categoryModel,
                            'galleryModel' => $galleryModel
                ]);
            }
        } else {
            return $this->redirect(\Yii::$app->urlManager->createUrl('site/index'));
        }
    }

    public function actionRate() {
        $rateModel = new Rates();
        $site_id = Yii::$app->request->get()['id'];
        if (!\Yii::$app->user->isGuest) {

            $rate = Rates::findAll(["site_id" => Yii::$app->request->get()['id'], "user_id" => \Yii::$app->user->identity->id]);
            if (count($rate) > 0) {
                $rateModel->deleteAll(["site_id" => Yii::$app->request->get()['id'], "user_id" => \Yii::$app->user->identity->id]);
                $result['rated'] = "rate";
            } else {

                $rateModel->user_id = \Yii::$app->user->identity->id;
                $rateModel->site_id = Yii::$app->request->get()['id'];
// 
                $rateModel->save();
                $result['rated'] = "unrate";
            }
            $result['id'] = Yii::$app->request->get()['id'];

        }else{
            $result['isGuest'] = true;
        }
            return json_encode($result);
    }

    /**
     * Deletes an existing WebSite model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $model = $this->findModel($id);

        if (!\Yii::$app->user->isGuest && \Yii::$app->user->identity->id == $model->creator_id) {
            $this->findModel($id)->delete();
            return $this->redirect(['index']);
        } else {
            return $this->redirect(\Yii::$app->urlManager->createUrl('site/index'));
        }
    }

    /**
     * Finds the WebSite model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return WebSite the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = WebSite::findOne($id)) !== null) {

            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
