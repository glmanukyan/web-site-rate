<?php

namespace frontend\controllers;

use Yii;
use common\models\LoginForm;
use frontend\models\SignupForm;
use yii\web\Controller;
use common\models\User;
use lajax\translatemanager\helpers\Language;

/**
 * Site controller
 */
class BaseController extends \yii\web\Controller {

    /**
     * @inheritdoc
     */
    public $loginFormModel;
    public $signupFormModel;
    public $userModel;

    public function init() {
        Language::registerAssets();
        parent::init();
    }

    public function __construct($id, $module, $config = []) {
        $this->id = $id;
        $this->module = $module;


        $this->loginFormModel = new LoginForm();
        $this->signupFormModel = new SignupForm();
        $this->userModel = new User();
    }

}
