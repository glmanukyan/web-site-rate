<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use yii\helpers\Url;
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0">

        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <link rel="apple-touch-icon-precomposed" sizes="57x57" href="images/icons/apple-touch-57x57.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/icons/apple-touch-72x72.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/icons/apple-touch-114x114.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/icons/apple-touch-144x144.png">

        <!-- jQUERY -->
        <script src="<?php echo Yii::$app->getUrlManager()->getBaseUrl(); ?>/design_assets/js/jquery-2.1.3.min.js"></script>

        <!-- BOOTSTRAP JS -->
        <script src="<?php echo Yii::$app->getUrlManager()->getBaseUrl(); ?>/design_assets/js/bootstrap.min.js"></script>

        <script type="text/javascript" src="<?php echo Yii::$app->getUrlManager()->getBaseUrl(); ?>/js/underscore-min.js"></script>
        <!--<script type="text/javascript" src="<?php // echo Yii::$app->getUrlManager()->getBaseUrl();         ?>/js/jquery-validate.js"></script>-->
        <script type="text/javascript" src="<?php echo Yii::$app->getUrlManager()->getBaseUrl(); ?>/uploadifive/jquery.uploadifive.js"></script>
        <!-- VIEWPORT -->
        <script src="<?php echo Yii::$app->getUrlManager()->getBaseUrl(); ?>/design_assets/js/viewport/jquery.viewport.js"></script>

        <!-- MENU -->
        <script src="<?php echo Yii::$app->getUrlManager()->getBaseUrl(); ?>/design_assets/js/menu/hoverIntent.js"></script>
        <script src="<?php echo Yii::$app->getUrlManager()->getBaseUrl(); ?>/design_assets/js/menu/superfish.js"></script>

        <!-- FANCYBOX -->
        <script src="<?php echo Yii::$app->getUrlManager()->getBaseUrl(); ?>/design_assets/js/fancybox/jquery.fancybox.pack.js"></script>

        <!-- REVOLUTION SLIDER -->
        <script src="<?php echo Yii::$app->getUrlManager()->getBaseUrl(); ?>/design_assets/js/revolutionslider/js/jquery.themepunch.tools.min.js"></script>
        <script src="<?php echo Yii::$app->getUrlManager()->getBaseUrl(); ?>/design_assets/js/revolutionslider/js/jquery.themepunch.revolution.min.js"></script>

        <!-- BxSLIDER -->
        <script src="<?php echo Yii::$app->getUrlManager()->getBaseUrl(); ?>/design_assets/js/bxslider/jquery.bxslider.min.js"></script>

        <!-- PARALLAX -->
        <script src="<?php echo Yii::$app->getUrlManager()->getBaseUrl(); ?>/design_assets/js/parallax/jquery.parallax-scroll.min.js"></script>

        <!-- ISOTOPE -->
        <script src="<?php echo Yii::$app->getUrlManager()->getBaseUrl(); ?>/design_assets/js/isotope/imagesloaded.pkgd.min.js"></script>
        <script src="<?php echo Yii::$app->getUrlManager()->getBaseUrl(); ?>/design_assets/js/isotope/isotope.pkgd.min.js"></script>

        <!-- PLACEHOLDER -->
        <script src="<?php echo Yii::$app->getUrlManager()->getBaseUrl(); ?>/design_assets/js/placeholders/jquery.placeholder.min.js"></script>

        <!-- CONTACT FORM VALIDATE & SUBMIT -->
        <script src="<?php echo Yii::$app->getUrlManager()->getBaseUrl(); ?>/design_assets/js/validate/jquery.validate.min.js"></script>
        <script src="<?php echo Yii::$app->getUrlManager()->getBaseUrl(); ?>/design_assets/js/submit/jquery.form.min.js"></script>

        <!-- GOOGLE MAPS -->
        <script src="http://maps.google.com/maps/api/js?sensor=false"></script>
        <script src="<?php echo Yii::$app->getUrlManager()->getBaseUrl(); ?>/design_assets/js/googlemaps/jquery.gmap.min.js"></script>

        <!-- CHARTS -->
        <script src="<?php echo Yii::$app->getUrlManager()->getBaseUrl(); ?>/design_assets/js/charts/chart.min.js"></script>
        <script src="<?php echo Yii::$app->getUrlManager()->getBaseUrl(); ?>/design_assets/js/charts/jquery.easypiechart.min.js"></script>

        <!-- COUNTER -->
        <script src="<?php echo Yii::$app->getUrlManager()->getBaseUrl(); ?>/design_assets/js/counter/jQuerySimpleCounter.js"></script>

        <!-- YOUTUBE PLAYER -->
        <script src="<?php echo Yii::$app->getUrlManager()->getBaseUrl(); ?>/design_assets/js/ytplayer/jquery.mb.YTPlayer.js"></script>

        <!-- TWITTER -->
        <script src="<?php echo Yii::$app->getUrlManager()->getBaseUrl(); ?>/design_assets/js/twitter/twitterfetcher.js"></script>

        <!-- ANIMATIONS -->
        <script src="<?php echo Yii::$app->getUrlManager()->getBaseUrl(); ?>/design_assets/js/animations/wow.min.js"></script>

        <!-- CUSTOM JS -->
        <script src="<?php echo Yii::$app->getUrlManager()->getBaseUrl(); ?>/design_assets/js/custom.js"></script>

        <script type="text/javascript" src="<?php echo Yii::$app->getUrlManager()->getBaseUrl(); ?>/js/main.js"></script>
        <!-- FONTS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic">
        <link href="<?php echo Yii::$app->getUrlManager()->getBaseUrl(); ?>/css/flags.css" rel="stylesheet">
        <link href="<?php echo Yii::$app->getUrlManager()->getBaseUrl(); ?>/css/language-picker.css" rel="stylesheet">
        <link href="<?php echo Yii::$app->getUrlManager()->getBaseUrl(); ?>/uploadifive/uploadifive.css" rel="stylesheet">

        <!-- BOOTSTRAP CSS -->
        <link rel="stylesheet" href="<?php echo Yii::$app->getUrlManager()->getBaseUrl(); ?>/design_assets/css/bootstrap.min.css"> 

        <!-- FONT AWESOME -->
        <link rel="stylesheet" href="<?php echo Yii::$app->getUrlManager()->getBaseUrl(); ?>/design_assets/fontawesome/css/font-awesome.min.css">

        <!-- MIU ICON FONT -->
        <link rel="stylesheet" href="<?php echo Yii::$app->getUrlManager()->getBaseUrl(); ?>/design_assets/miuiconfont/miuiconfont.css">

        <!-- FANCYBOX -->
        <link rel="stylesheet" href="<?php echo Yii::$app->getUrlManager()->getBaseUrl(); ?>/design_assets/js/fancybox/jquery.fancybox.css">

        <!-- REVOLUTION SLIDER -->
        <link rel="stylesheet" href="<?php echo Yii::$app->getUrlManager()->getBaseUrl(); ?>/design_assets/js/revolutionslider/css/settings.css">

        <!-- BxSLIDER -->
        <link rel="stylesheet" href="<?php echo Yii::$app->getUrlManager()->getBaseUrl(); ?>/design_assets/js/bxslider/jquery.bxslider.css">

        <!-- YOUTUBE PLAYER -->
        <link rel="stylesheet" href="<?php echo Yii::$app->getUrlManager()->getBaseUrl(); ?>/design_assets/js/ytplayer/css/YTPlayer.css">

        <!-- ANIMATIONS -->
        <link rel="stylesheet" href="<?php echo Yii::$app->getUrlManager()->getBaseUrl(); ?>/design_assets/js/animations/animate.min.css">

        <!-- CUSTOM & PAGES STYLE -->
        <link rel="stylesheet" href="<?php echo Yii::$app->getUrlManager()->getBaseUrl(); ?>/design_assets/css/custom.css">
        <link rel="stylesheet" href="<?php echo Yii::$app->getUrlManager()->getBaseUrl(); ?>/design_assets/css/pages-style.css">
        <link href="<?php echo Yii::$app->getUrlManager()->getBaseUrl(); ?>/css/site.css" rel="stylesheet">

        <?php $this->head() ?>

    </head>
    <body>
        <?php $this->beginBody() ?>
        <div id="page-wrapper">

            <!-- HEADER -->
            <header id="header">

                <div class="container">
                    <div class="row">
                        <div class="col-sm-2">

                            <!-- LOGO -->
                            <div id="logo">
                                <a href="<?php echo Yii::$app->getUrlManager()->getBaseUrl(); ?>">
                                    <img src="<?php echo Yii::$app->getUrlManager()->getBaseUrl(); ?>/images/logo.png" alt="">
                                </a>
                            </div><!-- logo -->

                        </div><!-- col -->
                        <div class="col-sm-10">

                            <!-- SEARCH -->
                            <div id="search-container">
                                <form id="search-form" name="search-form" method="get" action="#">
                                    <fieldset>
                                        <input type="text" name="search" placeholder="Enter your keyword here and then press enter...">
                                    </fieldset>
                                </form>
                            </div><!-- search-container -->

                            <a class="search-button" href="#"></a>

                            <!-- MENU --> 


                            <nav>
                        
                            <a id="mobile-menu-button" href="#"><i class="fa fa-bars"></i></a>
                                             
                            <ul class="menu clearfix" id="menu">
                                
                                <li><a href="<?php echo Url::to(['web-site/list']); ?>">Home</a></li>
                                <li><a href="<?php echo Url::to(['']); ?>">News</a></li>
                                <li><a href="<?php echo Url::to(['site/contact']); ?>">Contact</a></li>
                                <li><a href="<?php echo Url::to(['site/about']); ?>">About</a></li>
                                <?php    if (Yii::$app->user->isGuest) :
                                    ?>
                                <li class="dropdown">
                                	<a class="dropdown-toggle sf-with-ul" href="#" data-toggle="dropdown" aria-expanded="false">Sign Up<b class="caret"></b></a>
                                    <ul>
                                        <li><a href="<?php echo Url::to(['/site/personal-signup']); ?>">Sign Up</a></li>
                                        
                                        <?php 
//                                        echo yii\authclient\widgets\AuthChoice::widget([
//                                            'baseAuthUrl' => ['site/auth']
//                                            ]);
                                        ?>
                                    </ul>
                                </li>
                                <li class="dropdown">
                                	<a class="dropdown-toggle sf-with-ul" href="#" data-toggle="dropdown" aria-expanded="false">Sign In<b class="caret"></b></a>
                                    <ul>
                                        <li><a href="<?php echo Url::to(['/site/login']); ?>">Sign In</a></li>
                                        
                                        <?php echo yii\authclient\widgets\AuthChoice::widget([
                                            'baseAuthUrl' => ['site/auth']
                                            ]);?>
                                    </ul>
                                </li>
                                        <?php
                                else:
                                  ?>
                                <li class="dropdown">
                                	<a class="dropdown-toggle sf-with-ul" href="#" data-toggle="dropdown" aria-expanded="false"><?php echo Yii::$app->controller->userModel->getFirstName(Yii::$app->user->getId()); ?><b class="caret"></b></a>
                                    <ul>
                                        <?php 
                                            if(Yii::$app->controller->userModel->getUserType(Yii::$app->user->getId()) != 2):
                                        ?>
                                                
                                        <li><a href="<?php echo Url::to(['/user-profile/update','id'=>Yii::$app->user->getId()]); ?>">My Profile</a></li>
                                        <li><a href="<?php echo Url::to(['/web-site/index','id'=>Yii::$app->user->getId()]); ?>">My Web Sites</a></li>
                                                <?php
                                        endif; ?>
                                        <li><a href="<?php echo Url::to(['/site/logout','id'=>Yii::$app->user->getId()]); ?>">Sign Out</a></li>
                                        
                                        
                                    </ul>
                                </li>
                                      <?php  
                                endif;
                                ?>
                                             
                               
                                
                                                       
                               <div class = "navbar-text pull-right">
                                <?php
                                echo \lajax\languagepicker\widgets\LanguagePicker::widget([
                                    'itemTemplate' => '<li><a href="{link}" title="{language}"><i class="large {language}"></i> {name}</a></li>',
                                    'activeItemTemplate' => '<a href="{link}" title="{language}"><i class="large {language}"></i> {name}</a>',
                                    'parentTemplate' => '<div class="language-picker dropdown-list {size}"><div>{activeItem}<ul>{items}</ul></div></div>',
                                    'languageAsset' => 'lajax\languagepicker\bundles\LanguageLargeIconsAsset', // StyleSheets
                                    'languagePluginAsset' => 'lajax\languagepicker\bundles\LanguagePluginAsset', // JavaScripts
                                ]);
                                ?> 

                            </div>  
                            </ul>
                    	</nav>
                    
                    </div><!-- col -->
                </div><!-- row -->
            </div><!-- container -->    
                    
                        </div><!-- col -->
                    </div><!-- row -->
                </div><!-- container -->    

            </header><!-- HEADER -->

            <div class="container">
                <?php
//                echo Breadcrumbs::widget([
//                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
//                ])
//                echo Alert::widget()
                ?>
               
                <div class="content">
                    <?= $content ?>
                </div>
            </div>


            <footer class="footer">
                <div class="container">
                    <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

                    <p class="pull-right"><?= Yii::powered() ?></p>
                </div>
            </footer>

            <!-- GO TOP -->
            <a id="go-top"><i class="miu-icon-circle_arrow-up_glyph"></i></a>



            <?php $this->endBody() ?>
        </div>
    </body>
</html>
<?php // $this->endPage()   ?>
