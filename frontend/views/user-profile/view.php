<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\UserProfile */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'User Profiles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-profile-view">

    <!--<h1><?= Html::encode($this->title) ?></h1>-->

    <p>
        <?= Html::a('Personal Info', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
       
         <?= Html::a('My Websites', ['web-site/index', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
   
    </p>
<?php if($model->type==1): ?>
         <?php 
//    DetailView::widget([
//        'model' => $model,
//        'attributes' => [
//            'username',
//            'first_name',
//            'last_name',
//            'middle_name',
//            'email:email',
//            'country',
//            'city',
//            'agency_name',
//            'agency_link',
//            'agency_description',
//        ],
//    ]) 
        ?>
        <?php else: ?>
         <?php
//            DetailView::widget([
//        'model' => $model,
//        'attributes' => [
//            'username',
//            'first_name',
//            'last_name',
//            'middle_name',
//            'email:email',
//            'country',
//            'city',
//            
//        ],
//    ]) 
                ?>
        <?php endif; ?>
   

</div>
