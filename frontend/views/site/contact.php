<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use lajax\translatemanager\helpers\Language as Lx;

$this->title = 'Contact';
$this->params['breadcrumbs'][] = $this->title;
//var_dump(Yii::$app->language);die;
?>
<div class="container">
    <div class="row">
        <div class="col-sm-12">

            <div class="headline style-3">

                <h5>Say hello</h5>
                <h2>Contact our team</h2>
                <p> Temporibus autem quibusdam et aut officiis debitis aut rerum.</p>

            </div><!-- headline -->

        </div><!-- col -->
    </div><!-- row -->
</div><!-- container -->

<div class="container">
    <div class="row">
        <div class="col-md-offset-2 col-md-8 col-sm-12">

            <div class="row">
                <div class="col-sm-4">

                    <div class="widget widget-contact">

                        <ul>
                            <li>
                                <span>Address</span>
                                1713 Hide A Way Road<br>
                                San Jose, CA 95118
                            </li> 
                        </ul>

                    </div><!-- widget-contact -->

                </div><!-- col -->
                <div class="col-sm-4">

                    <div class="widget widget-contact">

                        <ul>
                            <li>
                                <span>Phone &amp; Fax</span>                                    
                                +408-267-8351<br>
                                +408-267-8344
                            </li>
                        </ul>

                    </div><!-- widget-contact -->

                </div><!-- col -->
                <div class="col-sm-4">

                    <div class="widget widget-contact">

                        <ul>
                            <li>
                                <span>E-mail</span>
                                <a href="mailto:support@milo.com">support@milo.com</a> <br>
                                <a href="mailto:office@milo.com">office@milo.com</a>
                            </li>
                        </ul>

                    </div><!-- widget-contact -->

                </div><!-- col -->
            </div><!-- row -->

        </div><!-- col -->
    </div><!-- row -->
</div><!-- container -->

<div class="container">
    <div class="row">
        <div class="col-md-offset-2 col-md-8 col-sm-12">
            <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>
            <fieldset>
                <div id="alert-area"></div>
                <?= $form->field($model, 'name')->textInput(['class' => 'col-xs-12','placeolder'=>'name']) ?>

                <?= $form->field($model, 'email')->textInput(['class' => 'col-xs-12','placeolder'=>'email']) ?>

                <?= $form->field($model, 'subject')->textInput(['class' => 'col-xs-12','placeolder'=>'subject']) ?>

                <?= $form->field($model, 'body')->textArea(['rows' => 6,'class'=>'col-xs-12','placeolder'=>'message']) ?>

                <?php
//                echo $form->field($model, 'verifyCode')->widget(Captcha::className(), [
//                    'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
//                ])
                ?>

                <div class="form-group">
                    <?= Html::submitButton('Submit', ['class' => 'btn btn-default', 'name' => 'contact-button']) ?>
                </div>    

                <!--<form id="contact-form" name="contact-form" action="assets/php/send.php" method="post">-->


                
            </fieldset>
            <!--</form>-->

            <?php ActiveForm::end(); ?>
        </div><!-- col -->
    </div><!-- row -->
</div><!-- container -->

<div class="map" style="margin-bottom:0;"></div>
