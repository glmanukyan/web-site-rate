<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $model common\models\WebSite */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Web Sites', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="web-site-view">
    <div id="page-header">  
        <div class="container">
            <div class="row">
                <div class="col-sm-6">

                    <h4><?= Html::encode($this->title) ?></h4>

                </div>
                <div class="col-sm-6">
                    <?=
                    Breadcrumbs::widget([
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    ])
                    ?>

                </div><!-- row -->
            </div><!-- ontainer -->    
        </div><!-- page-header -->

        <div class="container">
            <div class="row">
                <div class="col-sm-12">

                    <div class="headline style-1">

                        <h4>Corporate identity</h4>
                        <h2>Wine branding</h2>
                    </div><!-- headline -->

                </div><!-- col -->
            </div><!-- row -->
        </div><!-- ontainer -->

        <div class="container">
            <div class="row">
                <div class="col-sm-4">

                    <ul class="project-details">
                        <li><strong>Title:</strong> <small><?php echo $model['title'] ?></small>
                        <li><strong>Date:</strong> <small><?php echo $model['id'] ?></small>
                        <li><strong>Webpage:</strong> <small class="text-lowercase"><a href="http://<?php echo $model['link'] ?>" target="_blank"><?php echo $model['link'] ?></a></small>
                        <li><strong>Category:</strong> <small><?php echo $model['category']->name ?></small>
                    </ul>

                </div><!-- col -->
                <div class="col-sm-8">

                    <h3><strong>Description</strong></h3>

                    <br>

                    <p><?php echo $model['description'] ?></p>

                    <!--                    </div> col 
                                        <div class="col-sm-4">-->

<!--                        <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure 
dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non 
proident, sunt in culpa qui officia deserunt dolor sit amet.</p>
                    -->
                </div><!-- col -->
            </div><!-- row -->
        </div><!-- ontainer -->

        <br><br><br>
        <?php if (count($model) > 0): ?>


            <div class="container">
                <div class="row">
                    <div class="col-sm-12">

                        <div class="images-slider-2">
                            <ul>
                                <?php foreach ($model['image'] as $image): ?>
                                    <?php // var_dump($model['image']);die;  ?>
                                    <li><?= Html::img('/uploads/web_site_images/' . $image->image); ?></li>
                                <?php endforeach; ?>

                            </ul>
                        </div><!-- images-slider -->

                    </div><!-- col -->
                </div><!-- row -->
            </div><!-- ontainer -->

        <?php endif; ?>


        <div class="container">
            <div class="row pull-right">
                <p>
                    <?= Html::a('Update ' . $model['title'], ['update', 'id' => $model->id], ['class' => 'btn btn-default']) ?>
                    <?=
                    Html::a('Delete ' . $model['title'], ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-default',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ])
                    ?>
                </p>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-sm-12">

                    <div class="headline style-3">

                        <h5>All in one</h5>
                        <h2>Related projects</h2>
                        <p>Temporibus autem quibusdam et aut officiis debitis aut rerum.</p>

                    </div><!-- headline -->

                </div><!-- col -->
            </div><!-- row -->
        </div><!-- container -->
        <?php if(count($model['relatedSites'])>0):?>
            
        
        <div class="container">
            <div class="row">
                
                <div class="col-sm-4">

                    <div class="portfolio-item wow fadeInDown">

                        <div class="portfolio-item-thumbnail">

                            <img src="images/portfolio/image-8.jpg" alt="">

                            <div class="portfolio-item-hover">

                                <div class="portfolio-item-description">

                                    <h3><a href="portfolio-single.html">Business card</a></h3>
                                    <h5>Industrial design</h5>

                                </div><!-- portfolio-item-description -->

                                <a class="fancybox-portfolio-gallery zoom-action" href="images/portfolio/image-8.jpg"><i class="fa fa-plus"></i></a>

                            </div><!-- portfolio-item-hover -->

                        </div><!-- portfolio-item-thumbnail -->

                    </div><!-- portfolio-item -->

                </div><!-- col -->

            </div><!-- row -->
        </div><!-- container -->
        <?php endif; ?>
        <section class="full-section parallax" id="section-4">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">

                        <p class="text-center"><a class="btn btn-default" href="<?php echo \yii\helpers\Url::to(["site/contact"]); ?>">Contact us<i class="fa fa-arrow-right"></i></a></p>

                    </div><!-- col -->
                </div><!-- row -->
            </div><!-- ontainer -->
        </section><!-- full-section -->





    </div>
</div>
