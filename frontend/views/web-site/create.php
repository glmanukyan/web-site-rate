<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\WebSite */

$this->title = 'Create Web Site';
$this->params['breadcrumbs'][] = ['label' => 'Web Sites', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <div class="row">
        <div class="col-md-offset-2 col-md-8 col-sm-12">
            <div class="web-site-create">

                <h1><?= Html::encode($this->title) ?></h1>

                <?=
                $this->render('_form', [
                    'model' => $model,
                    'categoryModel' => $categoryModel,
                    'galleryModel' => $galleryModel
                ])
                ?>

            </div>
        </div>
    </div>
</div>
