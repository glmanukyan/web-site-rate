<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use yii\helpers\ArrayHelper;

use dosamigos\fileupload\FileUploadUI;
use kartik\widgets\FileInput;
/* @var $this yii\web\View */
/* @var $model common\models\WebSite */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="web-site-form" id="upImages">

    <?php $form = ActiveForm::begin(["id" => "web-site",
            'options' => ['enctype' => 'multipart/form-data'] // important
        ]); ?>
     <fieldset>
    <?= $form->field($model, 'title')->textInput(['maxlength' => true,'class' => 'col-xs-12']) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true,'class' => 'col-xs-12']) ?>

    <?= $form->field($model, 'link')->textInput(['maxlength' => true,'class' => 'col-xs-12']) ?>

<?= Html::activeDropDownList($model, 'category_id',
      ArrayHelper::map($categoryModel, 'id', 'name') ,['prompt'=>'--Select Syte Category--','class'=>'col-xs-12']) ?>
      <?php
    if (is_array($galleryModel)):
        foreach ($galleryModel as $image):
            ?>
            <div class="pull-left uploaded_image">
                <?php // echo Html::img('@web/uploads/web_site_images/'. $image->image, ['class' => 'img-thumbnail pull-left img-responsive']); ?>
                
                <img class="img-thumbnail" src="<?php echo "/uploads/web_site_images/" . $image->image; ?>">
                <span class="delete_uploaded_image" data-id="<?php echo $image->id; ?>" data-image="<?php echo $image->image; ?>">x</span>

                <?= $form->field($image, 'main')->radio(["class" => "is-main", "data-image" => $image->image]); ?>
            </div>
        <?php endforeach; ?>
        <div class="clearfix"></div>
        <?php
        echo $form->field($image, 'uploadImage')->widget(FileInput::classname(), [
            'options' => ['accept' => 'image/*'],
            'pluginOptions' => ['allowedFileExtensions' => ['jpg', 'gif', 'png']]
        ]);
        ?>
        <?= $form->field($galleryModel[0], 'mainImage')->hiddenInput()->label(false); ?>
        <?= $form->field($galleryModel[0], 'uploadedFiles')->hiddenInput()->label(false); ?>
        <?= $form->field($galleryModel[0], 'deletedFiles')->hiddenInput()->label(false); ?>
        <?= $form->field($galleryModel[0], 'deleteUploadedImages')->hiddenInput()->label(false); ?>

        <?php
    else:
        ?>
        <?php
        echo $form->field($galleryModel, 'uploadImage')->widget(FileInput::classname(), [
            'options' => ['accept' => 'image/*'],
            'pluginOptions' => ['allowedFileExtensions' => ['jpg', 'gif', 'png']]
        ]);
        ?>
        <?= $form->field($galleryModel, 'mainImage')->hiddenInput()->label(false); ?>
        <?= $form->field($galleryModel, 'uploadedFiles')->hiddenInput()->label(false); ?>
        <?= $form->field($galleryModel, 'deletedFiles')->hiddenInput()->label(false); ?>
        <?= $form->field($galleryModel, 'deleteUploadedImages')->hiddenInput()->label(false); ?>

    <?php endif; ?>



    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
 </fieldset>
    <?php ActiveForm::end(); ?>

</div>
<script type="text/javascript">
<?php
$timestamp = time();
$basePath = Yii::$app->getBasePath()
?>
    $(document).ready(function () {
        var images = [];
        var image;
        var i = 0;
        var j = 0;
        
        $('#websitegallery-uploadimage').uploadifive({
            'auto': true,
            'multiple': false,
            'checkScript': '<?php echo Yii::$app->getUrlManager()->getBaseUrl(); ?>/uploadifive/check-exists.php',
            'formData': {
                'timestamp': '<?php echo $timestamp; ?>',
                'token': '<?php echo md5('unique_salt' . $timestamp); ?>',
                'uploadDir': 'frontend/web/uploads/images/',
               
            },
//          'queueID': 'queue',
            'uploadScript': '<?php echo Yii::$app->getUrlManager()->getBaseUrl(); ?>/uploadifive/uploadifive.php',
            'onUploadComplete': function (file, data) {
                var image = jQuery.parseJSON(data);
                image.i = i;
                images.push(image);
                var template = _.template($('#img_complete_tpl').html())({
                    dataImg: image.fileName
                });

                $("#uploadifive-websitegallery-uploadimage-file-" + i).prepend(_.template(template));
                i++;
            }
        });
        var deleteUploadedImages = [];
        $("body").on("click", ".delete_uploaded_image", function (event) {
            event.preventDefault();
            var deletedImage = $(this);

            deleteUploadedImages.push({"id": $(deletedImage).data("id"), "image": $(deletedImage).data("image")});
            $(deletedImage).parent().remove();
        });
        $("body").on("submit", "#web-site", function (event) {
            var idArray = [];
            var deletedImages = [];
            var completetdId;
            var uploadedImages = [];
            $("body .complete").each(function () {
                idArray.push(this.id.substr(44));
            });
            $.each(images, function () { 
                if (jQuery.inArray(this.i.toString(), idArray) !== -1) {
                    uploadedImages.push(this);
             
                } else {
                    deletedImages.push(this);
                }
            });
            
           
            $("#websitegallery-mainimage").val($('input[class=is-main]:checked', "#web-site").data("image"));
            $("#websitegallery-uploadedfiles").val(JSON.stringify(uploadedImages));
            $("#websitegallery-deletedfiles").val(JSON.stringify(deletedImages));
            $("#websitegallery-deleteuploadedimages").val(JSON.stringify(deleteUploadedImages));
            return;
        });

    });
</script>

<script type="text/template" id='img_complete_tpl'>

    <img class="img-thumbnail" src="/uploads/images/<%= dataImg %>">
   
    <input type="radio" id="websitegallery-main" class="is-main" name="WebSiteGallery[main]" value="" checked="" data-image="<%= dataImg %>"> Main</label>

</script>