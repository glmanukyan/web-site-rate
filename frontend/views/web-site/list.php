<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\WebSite */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Web Sites';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="web-site-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Web Site', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php // var_dump($dataProvider);die; ?>
    <?php
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
//            'id',
            [
                'format' => 'html',
                'label' => 'Main Image',
                'contentOptions' => ['class' => ''],
                'value' => function ($data) {
            if ($data['image']['image'] != null) {

                return Html::img('/uploads/web_site_images/' . $data['image']['image'], ['width' => '60px']);
            } else {
//                        sd
                return Html::img('/uploads/web_site_images/no_image.png', ['width' => '60px']);
            }
        },
            ],
            'title',
            'description',
            'link',
//            'category_id',
            'rate',
       
            'category.name',
           
            [
                'format' => 'html',
                'label' => 'Main Image',
                'value' => function ($data) {

                    $rated = 0;
                    foreach ($data['rates'] as $rate) {
                        if (!\Yii::$app->user->isGuest && $rate['user_id'] == \Yii::$app->user->identity->id) {
                            $rated = 1;
                        }
                    }
                    if ($rated == 1) {
                        return Html::a('Unrate', ['web-site/rate', 'id' => $data['id']], [
                                    'class' => 'rate btn btn-danger'
                        ]);
                    } else {
                        return Html::a('Rate', ['web-site/rate', 'id' => $data['id']], ['class' => 'rate btn btn-primary']);
                    }
                },
                    ],
                          [
                'format' => 'html',
                'label' => 'Main Image',
                'value' => function ($data) {

                    
                   return count($data['rates']);
                },
                    ],
                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]);
            ?>

    <script type="text/javascript">
        $(document).ready(function () {
            $("body").on("click", ".rate", function (event) {
                event.preventDefault();
                var rateClass = "";
                var curSite = $(this);
                if ($(curSite).hasClass("btn-primary")) {
                    rateClass = " rate btn btn-danger";
                } else {
                    rateClass = "rate btn btn-primary";
                }
                $.ajax({
                    url: $(this).attr('href'),
                    method: "GET"
                }).done(function (response) {
                    var result = jQuery.parseJSON(response);
                    if(result.isGuest){
                        alert("please login");
                        return false;
                    };
                    var template = _.template($('#rate_tpl').html())({
                        rateClass: result.rated,
                        siteId: result.id
                    });

                    $(curSite).parent().html(_.template(template));


                }).fail(function () {

                });
            });
        });
                </script>

    <script type = "text/template" id = 'rate_tpl' >
                <a class = "rate btn <%= (rateClass=='rate')?'btn-primary':'btn-danger'%> " href = "/web-site/rate?id=<%= siteId %>" > <%= (rateClass=='rate')?'Rate':'Unrate'%> </a>
    </script>