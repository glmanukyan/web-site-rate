<?php

/*
  UploadiFive
  Copyright (c) 2012 Reactive Apps, Ronnie Garcia
 */

// Set the uplaod directory
$uploadPath = $_POST['uploadDir'];
// Set the allowed file extensions
$fileTypes = array('jpg', 'jpeg', 'gif', 'png'); // Allowed file extensions

$verifyToken = md5('unique_salt' . $_POST['timestamp']);

if (!empty($_FILES) && $_POST['token'] == $verifyToken) {
    $tempFile = $_FILES['Filedata']['tmp_name'];
    $uploadDir = $_SERVER['DOCUMENT_ROOT'] . $uploadPath;
    $fileName = time() . "_" . $_FILES['Filedata']['name'];
    $targetFile = $uploadDir . $fileName;
    // Validate the filetype
    $fileParts = pathinfo($_FILES['Filedata']['name']);

    if (in_array(strtolower($fileParts['extension']), $fileTypes)) {
       
        $uploaded_files["fileName"] = $fileName;
        $uploaded_files["targetFile"] = $targetFile;


        if (move_uploaded_file($tempFile, $targetFile)) {
            echo json_encode($uploaded_files);
        } else {
            echo 2;
        }
    } else {
        // The file type wasn't allowed
        echo 'Invalid file type.';
    }
}
?>