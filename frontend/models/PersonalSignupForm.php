<?php

namespace frontend\models;

use common\models\User;
use yii\base\Model;
use Yii;

/**
 * Signup form
 */
class PersonalSignupForm extends Model {

    public $first_name;
    public $last_name;
    public $middle_name;
    public $city;
    public $country;
    public $type = 3;
    public $username;
    public $email;
    public $password;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['first_name', 'last_name', 'middle_name'], 'filter', 'filter' => 'trim'],
            [['first_name', 'last_name', 'middle_name'], 'required'],
            [['first_name', 'last_name', 'middle_name','country','city'], 'string', 'min' => 2, 'max' => 255],
            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],
            ['password', 'required'],
            ['password', 'string', 'min' => 6],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup() {
        if ($this->validate()) {
            $user = new User();
            $user->first_name = $this->first_name;
            $user->last_name = $this->last_name;
            $user->middle_name = $this->middle_name;
            $user->username = $this->username;
            $user->email = $this->email;
            $user->country = $this->country;
            $user->city = $this->city;
            $user->type = $this->type;
            $user->setPassword($this->password);
            $user->generateAuthKey();
            if ($user->save()) {
                return $user;
            }
        }

        return null;
    }

}
