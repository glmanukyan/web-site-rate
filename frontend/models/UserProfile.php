<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $username
 * @property string $first_name
 * @property string $last_name
 * @property string $middle_name
 * @property string $country
 * @property string $city
 * @property string $agency_name
 * @property string $agency_link
 * @property string $agency_description
 * @property integer $locked
 * @property integer $type
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $fb_id
 * @property string $fb_name
 * @property string $google_id
 * @property string $google_name
 * @property string $vk_id
 * @property string $vk_name
 */
class UserProfile extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'auth_key', 'password_hash', 'email', 'created_at', 'updated_at'], 'required'],
            [['locked', 'type', 'status', 'created_at', 'updated_at'], 'integer'],
            [['username', 'first_name', 'last_name', 'middle_name', 'country', 'city', 'agency_name', 'agency_link', 'agency_description', 'password_hash', 'password_reset_token', 'email', 'fb_id', 'fb_name', 'google_id', 'google_name', 'vk_id', 'vk_name'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['username'], 'unique'],
            [['email'], 'unique'],
            [['password_reset_token'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'middle_name' => 'Middle Name',
            'country' => 'Country',
            'city' => 'City',
            'agency_name' => 'Agency Name',
            'agency_link' => 'Agency Link',
            'agency_description' => 'Agency Description',
            'locked' => 'Locked',
            'type' => 'Type',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'email' => 'Email',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'fb_id' => 'Fb ID',
            'fb_name' => 'Fb Name',
            'google_id' => 'Google ID',
            'google_name' => 'Google Name',
            'vk_id' => 'Vk ID',
            'vk_name' => 'Vk Name',
        ];
    }
}
