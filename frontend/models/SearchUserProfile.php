<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\UserProfile;

/**
 * SearchUserProfile represents the model behind the search form about `app\models\UserProfile`.
 */
class SearchUserProfile extends UserProfile
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'locked', 'type', 'status', 'created_at', 'updated_at'], 'integer'],
            [['username', 'first_name', 'last_name', 'middle_name', 'country', 'city', 'agency_name', 'agency_link', 'agency_description', 'auth_key', 'password_hash', 'password_reset_token', 'email', 'fb_id', 'fb_name', 'google_id', 'google_name', 'vk_id', 'vk_name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UserProfile::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'locked' => $this->locked,
            'type' => $this->type,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'first_name', $this->first_name])
            ->andFilterWhere(['like', 'last_name', $this->last_name])
            ->andFilterWhere(['like', 'middle_name', $this->middle_name])
            ->andFilterWhere(['like', 'country', $this->country])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'agency_name', $this->agency_name])
            ->andFilterWhere(['like', 'agency_link', $this->agency_link])
            ->andFilterWhere(['like', 'agency_description', $this->agency_description])
            ->andFilterWhere(['like', 'auth_key', $this->auth_key])
            ->andFilterWhere(['like', 'password_hash', $this->password_hash])
            ->andFilterWhere(['like', 'password_reset_token', $this->password_reset_token])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'fb_id', $this->fb_id])
            ->andFilterWhere(['like', 'fb_name', $this->fb_name])
            ->andFilterWhere(['like', 'google_id', $this->google_id])
            ->andFilterWhere(['like', 'google_name', $this->google_name])
            ->andFilterWhere(['like', 'vk_id', $this->vk_id])
            ->andFilterWhere(['like', 'vk_name', $this->vk_name]);

        return $dataProvider;
    }
}
