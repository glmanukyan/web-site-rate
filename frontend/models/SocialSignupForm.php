<?php
namespace frontend\models;

use common\models\User;
use yii\base\Model;
use Yii;

/**
 * Signup form
 */
class SocialSignupForm extends Model
{
    public $fb_id;
    public $fb_name;
    public $google_id;
    public $google_name;
    public $vk_id;
    public $vk_name;
    public $type = 2;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
       
        if ($this->validate()) {
            $user = new User();
           
            $user->fb_id = $this->fb_id;
            $user->fb_name = $this->fb_name;
            $user->google_id = $this->google_id;
            $user->google_name = $this->google_name;
            $user->vk_id = $this->vk_id;
            $user->vk_name = $this->vk_name;
            $user->type = $this->type;
            
            if ($user->save()) {
            var_dump($user);die;
                return $user;
            }
        }

        return null;
    }
}
