<?php

use yii\db\Schema;
use yii\db\Migration;

class m150914_073255_rates extends Migration
{
   public function up() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
          
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%rates}}', [
            'id' => $this->primaryKey(),
            'site_id' => $this->integer()->notNull(),
            'user_id' => $this->integer()->notNull()
                ], $tableOptions);

        $this->addForeignKey('rate_fk', 'rates', 'site_id', 'web_site', 'id', 'CASCADE', 'NO ACTION');
    }

    public function down() {
        $this->dropTable('{{%rates}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
