<?php

use yii\db\Schema;
use yii\db\Migration;

class m150911_074637_web_site_gallery extends Migration
{
   public function up() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
          
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%web_site_gallery}}', [
            'id' => $this->primaryKey(),
            'site_id' => $this->integer()->notNull(),
            'image' => $this->string()->defaultValue(null),
            'main' => $this->boolean()->defaultValue(null)
                ], $tableOptions);

        $this->addForeignKey('gallery_fk', 'web_site_gallery', 'site_id', 'web_site', 'id', 'CASCADE', 'NO ACTION');
    }

    public function down() {
        $this->dropTable('{{%web_site_gallery}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
