<?php

use yii\db\Schema;
use yii\db\Migration;

class m130524_201442_init extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
           
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull()->unique(),
            'first_name' => $this->string(),
            'last_name' => $this->string(),
            'middle_name' => $this->string(),
            'country' => $this->string(),
            'city' => $this->string(),
            'agency_name' => $this->string(),
            'agency_link' => $this->string(),
            'agency_description' => $this->string(),
            'locked' => $this->boolean(),
            'type'=> $this->integer()->notNull(),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'email' => $this->string()->notNull()->unique(),
            'status' => $this->smallInteger()->notNull()->defaultValue(10),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'fb_id' => $this->string(),
            'fb_name' => $this->string(),
            'google_id' => $this->string(),
            'google_name' => $this->string(),
            'vk_id' => $this->string(),
            'vk_name' => $this->string()
        ]);
         $this->insert('user', [
            'first_name' => "Super Admin",
            'username' => 'super_admin',
            'email'=>'admin@admin.com',
            'type'=>4,
            'password_hash'=>'$2y$13$93aWrvYBdLOkrMKwfempf.7GSHp9VjT7zkechStCiQU0CDEvd7JHe',
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%user}}');
    }
}
