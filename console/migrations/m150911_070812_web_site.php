<?php

use yii\db\Schema;
use yii\db\Migration;

class m150911_070812_web_site extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
           
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%web_site}}', [
            'id' => $this->primaryKey(),
            
            'title' => $this->string()->notNull(),
            'description' => $this->string()->notNull(),
            'link' => $this->string()->notNull(),
            'category_id' => $this->integer()->notNull(),
            'rate' => $this->integer(),
            'view' => $this->integer(),
            'creator_id' => $this->integer()->notNull(),
        ], $tableOptions);
        
        $this->addForeignKey('user_site_fk', 'web_site', 'creator_id', 'user', 'id', 'CASCADE', 'NO ACTION');
    }

    public function down()
    {
        $this->dropTable('{{%web_site}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
