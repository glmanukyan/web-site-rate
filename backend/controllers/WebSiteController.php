<?php

namespace backend\controllers;

use Yii;
use common\models\WebSite;
use app\models\SearchWebSite;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * WebSiteController implements the CRUD actions for WebSite model.
 */
class WebSiteController extends BaseController {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all WebSite models.
     * @return mixed
     */
    public function actionIndex() {
//        $imagesPath = ;

        $searchModel = new SearchWebSite();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $siteModel = new WebSite;

        $imagesPath = $siteModel->getImageBasePath();
       
        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'imagesPath' => Yii::getAlias('@uploads_path'),
        ]);
    }

    /**
     * Displays a single WebSite model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new WebSite model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new WebSite();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing WebSite model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $galleryModel = WebSiteGallery::findAll(["site_id" => $id]);
        if (!\Yii::$app->user->isGuest && \Yii::$app->user->identity->id == $model->creator_id) {
            $categoryModel = Category::find()->all();
            if ($galleryModel == null) {
                $galleryModel = new WebSiteGallery();
            }
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                if (Yii::$app->request->post()["WebSiteGallery"]) {


                    $uploadedImages = json_decode(Yii::$app->request->post()["WebSiteGallery"]["uploadedFiles"]);
                    $deletedImages = json_decode(Yii::$app->request->post()["WebSiteGallery"]["deletedFiles"]);
                    $deleteUploadedImages = json_decode(Yii::$app->request->post()["WebSiteGallery"]["deleteUploadedImages"]);

                    if ($deletedImages != null) {
                        foreach ($deletedImages as $deletedImage) {
                            unlink($deletedImage->targetFile);
                        }
                    }
                    if ($deleteUploadedImages != null) {
                        foreach ($deleteUploadedImages as $deleteUploadedImage) {
                            WebSiteGallery::deleteAll(["id" => $deleteUploadedImage->id]);
                            unlink($_SERVER['DOCUMENT_ROOT'] . "uploads/web_site_images/" . $deleteUploadedImage->image);
                        }
                    }
                    if ($uploadedImages != null) {
                        foreach ($uploadedImages as $uploadedImage) {

                            $image = new WebSiteGallery();
                            $image->site_id = $id;
                            $image->image = $uploadedImage->fileName;
                            rename($uploadedImage->targetFile, $_SERVER['DOCUMENT_ROOT'] . "uploads/web_site_images/" . $uploadedImage->fileName);

                            $image->save();
                        }
                    }
                    $mainImage = Yii::$app->request->post()["WebSiteGallery"]["mainImage"];
                    $siteMainImage = WebSiteGallery::find()->where(['image' => $mainImage])->one();
//                var_dump($mainImageId);die;
                    if ($siteMainImage) {

                        $siteMainImage->main = 1;
                        $siteMainImage->save();
                    }
                }
                return $this->redirect(['view', 'id' => $model->id, 'categoryModel' => $categoryModel]);
            } else {
                return $this->render('update', [
                            'model' => $model,
                            'categoryModel' => $categoryModel,
                            'galleryModel' => $galleryModel
                ]);
            }
        } else {
            return $this->redirect(\Yii::$app->urlManager->createUrl('site/index'));
        }
    }

    /**
     * Deletes an existing WebSite model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the WebSite model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return WebSite the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = WebSite::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
