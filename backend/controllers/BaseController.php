<?php

namespace backend\controllers;

use Yii;
use common\models\LoginForm;
use yii\web\Controller;
use common\models\User;
use lajax\translatemanager\helpers\Language;


/**
 * Site controller
 */
class BaseController extends Controller {

    /**
     * @inheritdoc
     */
    public $loginFormModel;
    public $userModel;

    public function init() {
        Language::registerAssets();
        parent::init();
    }

    public function __construct($id, $module, $config = []) {
        $this->id = $id;
        $this->module = $module;


        $this->loginFormModel = new LoginForm();
        $this->userModel = new User();
    }
  

}
