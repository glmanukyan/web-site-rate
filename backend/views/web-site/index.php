<?php

use yii\helpers\Html;
use yii\grid\GridView;


/* @var $this yii\web\View */
/* @var $searchModel app\models\SearchWebSite */
/* @var $dataProvider yii\data\ActiveDataProvider */
//var_dump(Yii::getAlias('uploads_path'));die;
$this->title = 'Web Sites';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="web-site-index">
   
    <h1><?= Html::encode($this->title) ?></h1>
  
    <p>
        <?php // echo Html::a('Create Web Site', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
//            'id',
            [
                'format' => 'html',
                'label' => 'Main Image',
                'contentOptions' => ['class' => ''],
                'value' => function ($data) {
            $uploadPath = Yii::getAlias('@uploads_path');
            if ($data['image']['image'] != null) {
                
                return Html::img(Yii::$app->urlManagerFrontend->baseUrl . '/uploads/web_site_images/' . $data['image']['image'], ['width' => '60px']);
            } else {
                return Html::img(Yii::$app->urlManagerFrontend->baseUrl . '/uploads/web_site_images/no_image.png', ['width' => '60px']);
            }
        },
            ],
            'title',
            'description',
            'link',
            [
                'format' => 'html',
                'label' => 'Category',
                'contentOptions' => ['class' => ''],
                'value' =>  'category.name'
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);
    ?>

</div>
