<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use lajax\translatemanager\helpers\Language as Lx;
/* @var $this yii\web\View */
/* @var $model common\models\WebSite */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Web Sites', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="web-site-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>


    <h1><?php echo Lx::t('database', $model->title);?></h1>
    <h1><?php echo  $model->title;?></h1>


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'description',
            'link',
            'category_id',
            'rate',
            'view',
            'creator_id',
        ],
    ]) ?>

</div>
