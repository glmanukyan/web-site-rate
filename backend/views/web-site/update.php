<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\WebSite */

$this->title = 'Update Web Site: ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Web Sites', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="web-site-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
