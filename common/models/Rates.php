<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "rates".
 *
 * @property integer $id
 * @property integer $site_id
 * @property integer $user_id
 *
 * @property WebSite $site
 */
class Rates extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'rates';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['site_id', 'user_id'], 'required'],
            [['site_id', 'user_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'site_id' => 'Site ID',
            'user_id' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSite() {
        return $this->hasOne(WebSite::className(), ['id' => 'site_id']);
    }

     

}
