<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "web_site_gallery".
 *
 * @property integer $id
 * @property integer $site_id
 * @property string $image
 * @property integer $main
 *
 * @property WebSite $site
 */
class WebSiteGallery extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    
    public $uploadImage;
    public $uploadedFiles;
    public $deletedFiles;
    public $deleteUploadedImages;
    public $mainImage;
    public static function tableName()
    {
        return 'web_site_gallery';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['site_id'], 'required'],
            [['site_id', 'main'], 'integer'],
            [['image'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'site_id' => 'Site ID',
            'image' => 'Image',
            'main' => 'Main',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSite()
    {
        return $this->hasOne(WebSite::className(), ['id' => 'site_id']);
    }
}
