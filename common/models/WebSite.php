<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "web_site".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $link
 * @property integer $category_id
 * @property integer $rate
 * @property integer $view
 * @property integer $creator_id
 *
 * @property User $creator
 */
class WebSite extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public $main_image = 1;

    public static function tableName() {
        return 'web_site';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['title', 'description', 'link', 'category_id', 'creator_id'], 'required'],
            [['category_id', 'rate', 'view', 'creator_id'], 'integer'],
            [['title', 'description', 'link'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'link' => 'Link',
            'category_id' => 'Category ID',
            'rate' => 'Rate',
            'view' => 'View',
            'creator_id' => 'Creator ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreator() {
        return $this->hasOne(User::className(), ['id' => 'creator_id']);
    }

    public function getImage() {

        return $this->hasMany(WebSiteGallery::className(), ['site_id' => 'id']);
    }

    public function getCategory() {

        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }
    public function getRelatedSites() {

        return $this->hasMany(WebSite::className(), ['category_id' => 'category_id']);

    }

    public function getImageBasePath() {


        return __DIR__ . '/../../images/';
        
    }

    

    public function getRates() {
        return $this->hasMany(Rates::className(), ['site_id' => 'id']);
    }
    
  

}
