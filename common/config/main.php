<?php

return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'language'=>'en-US',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'urlManagerFrontend' => [
            'class' => 'yii\web\urlManager',
            'baseUrl' => '../../', //i.e. $_SERVER['DOCUMENT_ROOT'] .'/yiiapp/web/'
            'enablePrettyUrl' => true,
            'showScriptName' => false,
        ],
        'i18n' => [
            'translations' => [
                '*' => [
                    'class' => 'yii\i18n\DbMessageSource',
                    'db' => 'db',
                    'sourceLanguage' => 'xx-XX',
                    'sourceMessageTable' => '{{%language_source}}',
                    'messageTable' => '{{%language_translate}}',
            //      'cachingDuration' => 86400,
                    'enableCaching' => false,
                ],
            ],
        ],
        'authManager' => [
            'class' => 'yii\rbac\PhpManager',
            'class' => 'yii\rbac\DbManager',
        ],
       
    ],
    'modules' => [
        'translatemanager' => [
            'class' => 'lajax\translatemanager\Module',
            'root' => '@app', // The root directory of the project scan.
            'layout' => 'language', // Name of the used layout. If using own layout use 'null'.
//            'allowedIPs' => ['127.0.0.1'], // IP addresses from which the translation interface is accessible.
//            'roles' => ['@'], // For setting access levels to the translating interface.
//            'tmpDir' => '@runtime', // Writable directory for the client-side temporary language files. 
            // IMPORTANT: must be identical for all applications (the AssetsManager serves the JavaScript files containing language elements from this directory).
            'phpTranslators' => ['::t'], // list of the php function for translating messages.
            'jsTranslators' => ['lajax.t'], // list of the js function for translating messages.
            'patterns' => ['*.js', '*.php'], // list of file extensions that contain language elements.
//            'ignoredCategories' => ['yii'], // these categories won’t be included in the language database.
//            'ignoredItems' => ['config'], // these files will not be processed.
//            'scanTimeLimit' => null, // increase to prevent "Maximum execution time" errors, if null the default max_execution_time will be used
//            'searchEmptyCommand' => '!', // the search string to enter in the 'Translation' search field to find not yet translated items, set to null to disable this feature
            'defaultExportStatus' => 1, // the default selection of languages to export, set to 0 to select all languages by default
//            'defaultExportFormat' => 'json', // the default format for export, can be 'json' or 'xml'
            'tables' => [                   // Properties of individual tables
                [
                    'connection' => 'db', // connection identifier
                    'table' => '{{%web_site}}', // table name
                    'columns' => ['title', 'description'] //names of multilingual fields
                ],
                [
                    'connection' => 'db', // connection identifier
                    'table' => '{{%category}}', // table name
                    'columns' => ['name'] //names of multilingual fields
                ]
              
            ]
        ],
    ],
];
